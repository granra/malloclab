/*
 * mm.c -
 * 
 * In out not so naive approach, we will be implementing an explicit free list.
 * Blocks will contain headers and footers and a doubly linked list will be
 * implemented between free blocks by using the extra space in between to store
 * pointers to the payload of previous and next block.
 *
 * mm_init will create the initial empty heap of size 4kb and create a prologue
 * and an epilogue for it
 *
 * mm_free will free the corresponding block, then try coalescing to adjacent blocks.
 * Whether or not coalescing proves successful, the resulting free block will then be
 * added to the front of the free list.
 *
 * mm_alloc will scan through the free list using the best fit method. If however a
 * perfectly sized free block is found on the way then that block will be chosen for
 * allocation immediately. The free list will then be made sure to bypass the
 * allocated block and the block chosen will be returned. If no free space is found
 * the the heap will be extender by a free vlock of either 4kb in size, or if the
 * requested space was larger, the corresponding size. A portion or all of that
 * extended space will then be allocated and returned.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in below _AND_ in the
 * struct that follows.
 *
 * === User information ===
 * Group: BestBudz420
 * User 1: arnari13
 * SSN: 1205913209
 * User 2: atligud13
 * SSN: 1507922319
 * === End User Information ===
 ********************************************************/
team_t team = {
    /* Group name */
    "BestBudz420",
    /* First member's full name */
    "Arnar Gauti Ingason",
    /* First member's email address */
    "arnari13@ru.is",
    /* Second member's full name (leave blank if none) */
    "Atli Guðlaugsson",
    /* Second member's email address (leave blank if none) */
    "atligud13@ru.is",
    /* Leave blank */
    "",
    /* Leave blank */
    ""
};

extern int verbose;

/* Basic constants and macros */
#define WSIZE       4       /* word size (bytes) */  
#define DSIZE       8       /* doubleword size (bytes) */
#define CHUNKSIZE  (1<<10)  /* initial heap size (bytes) */
#define OVERHEAD    8       /* overhead of header and footer (bytes) */

#define MAX(x, y) ((x) > (y)? (x) : (y)) 

/* single word (4) or double word (8) alignment */
#define ALIGNMENT 8

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)


#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

/* Pack a size and allocated bit into a word */
#define PACK(size, alloc)  ((size) | (alloc))

/* Read and write a word at address p */
#define GET(p)       (*(size_t *)(p))
#define PUT(p, val)  (*(size_t *)(p) = (val))  

/* Read the size and allocated fields from address p */
#define GET_SIZE(p)  (GET(p) & ~0x7)
#define GET_ALLOC(p) (GET(p) & 0x1)

/* Given block ptr bp, compute address of its header and footer */
#define HDRP(bp)       ((char *)(bp) - WSIZE)  
#define FTRP(bp)       ((char *)(bp) + GET_SIZE(HDRP(bp)) - DSIZE)

/* Given block ptr bp, compute address of next and previous blocks */
#define NEXT_BLKP(bp)  ((char *)(bp) + GET_SIZE(((char *)(bp) - WSIZE)))
#define PREV_BLKP(bp)  ((char *)(bp) - GET_SIZE(((char *)(bp) - DSIZE)))


struct header {
	size_t size;
	struct header* next;
	struct header* prev;
};

/* Globals */
static char *heap_listp;

/* Helper functions */
void print_free_list();
void print_all_list();
static void *extend_heap(size_t words);
void add_to_list(char* ptr);
void remove_from_list(char* ptr);
void *find_fit(size_t size);
void split_block(void *ptr, size_t size);
static void *coalesce(void *ptr);
static void printblock(void *bp); 
static void checkblock(void *bp);
void mm_checkheap();

/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
	/* Get heap space for prologue and epilogue */
	if ((heap_listp = mem_sbrk(4*WSIZE)) == (void*)-1)
		return -1;
	
	/* Write prologue and epilogue to the heap */
	PUT(heap_listp, 0);
	PUT(heap_listp+WSIZE, PACK(OVERHEAD, 1));
	PUT(heap_listp+DSIZE, PACK(OVERHEAD, 1));
	PUT(heap_listp+WSIZE+DSIZE, PACK(0, 1));

	heap_listp = NULL;

	/* Make initial 4kb block between the prologue and epilogue */
	if (extend_heap(CHUNKSIZE/WSIZE) == NULL)
		return -1;	

    return 0;
}


/* 
 * mm_malloc - Allocate a block by looking through the free list.
 *     If no suitable block in the free list is found then the heap
 *     is extended.
 *     Always allocate a block whose size is a multiple of the alignment.
 */
void *mm_malloc(size_t size)
{
	if (verbose == 2) printf("Entering malloc with size %u\n", size);

    int newsize = ALIGN(size + OVERHEAD);
	void *newblock = find_fit(newsize);

	/* No suitable block was found */
	if (newblock == NULL)
		newblock = extend_heap(MAX(CHUNKSIZE, newsize) / WSIZE);
	
	/* extend_heap failed */
	if (newblock == (void*)-1)
		return NULL;

	/* The new block is bigger than requested */
	if (GET_SIZE(HDRP(newblock)) > newsize)
		split_block(newblock, newsize);

	/* Remove block from free list */
	remove_from_list(HDRP(newblock));

	/* Marking block as allocated */
	PUT(HDRP(newblock), PACK(GET_SIZE(HDRP(newblock)), 1));
	PUT(FTRP(newblock), PACK(GET_SIZE(HDRP(newblock)), 1));

	if (verbose == 2) mm_checkheap();

	return newblock;
}

/*
 * mm_free - Freeing a block by marking it as free in the header and footer
 *     and adding it to the free list.
 */
void mm_free(void *ptr)
{
	if (verbose == 2) printf("Freeing block of size %u\n", GET_SIZE(HDRP(ptr)));

    /* Mark block as free */
    PUT(HDRP(ptr), PACK(GET_SIZE(HDRP(ptr)), 0));
    PUT(FTRP(ptr), PACK(GET_SIZE(HDRP(ptr)), 0));

    /* Adding block to free list*/
    add_to_list(HDRP(ptr));

    /* Coalescing */
    coalesce(ptr);

	if (verbose == 2) mm_checkheap();

    return;
}

/*
 * mm_realloc - naive implementation of mm_realloc
 */
void *mm_realloc(void *ptr, size_t size)
{
	if (verbose == 2) printf("Entering realloc with size %u\n", size);

    void *newp;
    size_t copySize;
	size_t newblocksize = ALIGN(size + OVERHEAD);
	
	/* realloc smaller block */
	if ((int)(GET_SIZE(HDRP(ptr)) - newblocksize) >= 2*DSIZE) {
		size_t latter = GET_SIZE(HDRP(ptr)) - newblocksize;

		/* packing new smaller block */
		PUT(HDRP(ptr), PACK(newblocksize, 1));
		PUT(FTRP(ptr), PACK(newblocksize, 1));
		/* packing remainder of old block as free */
		PUT(HDRP(NEXT_BLKP(ptr)), PACK(latter, 0));
		PUT(FTRP(NEXT_BLKP(ptr)), PACK(latter, 0));
		add_to_list(HDRP(NEXT_BLKP(ptr)));

		coalesce(NEXT_BLKP(ptr));

		return ptr;
	}
	/* next block in memory is free */
	else if (!GET_ALLOC(HDRP(NEXT_BLKP(ptr)))) {

		size_t sum = GET_SIZE(HDRP(ptr)) + GET_SIZE(HDRP(NEXT_BLKP(ptr)));
	
		/* sum of current block and next block
		 * is large enough to store new payload */
		if (sum >= newblocksize) {

			remove_from_list(HDRP(NEXT_BLKP(ptr)));
			
			/* block will be split */
			if (sum - newblocksize >= 2*DSIZE) {
				PUT(HDRP(ptr), PACK(newblocksize, 1));
				PUT(FTRP(ptr), PACK(newblocksize, 1));
				PUT(HDRP(NEXT_BLKP(ptr)), PACK(sum - newblocksize, 0));
				PUT(FTRP(NEXT_BLKP(ptr)), PACK(sum - newblocksize, 0));
				add_to_list(HDRP(NEXT_BLKP(ptr)));
				coalesce(NEXT_BLKP(ptr));
			}
			/* two adjacent blocks will become one */
			else {
				PUT(HDRP(ptr), PACK(sum, 1));
				PUT(FTRP(ptr), PACK(sum, 1));
			}

			return ptr;
		}

	}

	/* new block must be malloc'd and contents copied over */
    if ((newp = mm_malloc(size)) == NULL) {
        printf("ERROR: mm_malloc failed in mm_realloc\n");
        exit(1);
    }
    copySize = GET_SIZE(HDRP(ptr));
    if (size < copySize)
      copySize = size;
    memcpy(newp, ptr, copySize);
    mm_free(ptr);

	if (verbose == 2) mm_checkheap();

    return newp;
}

/*
 * extend_heap - Extends the heap by given amount of words.
 *     The new space on the heap becomes a block which is added
 *     in front of the free list.
 */
static void *extend_heap(size_t words) 
{
    char *bp;
    size_t size;
	
    /* Allocate an even number of words to maintain alignment */
    size = (words % 2) ? (words+1) * WSIZE : words * WSIZE;
    if ((bp = mem_sbrk(size)) == (void *)-1) 
	return NULL;
	
    /* Initialize free block header/footer and the epilogue header */
    PUT(HDRP(bp), PACK(size, 0));         /* free block header */
    PUT(FTRP(bp), PACK(size, 0));         /* free block footer */
    PUT(HDRP(NEXT_BLKP(bp)), PACK(0, 1)); /* new epilogue header */

    /* Adding new block into front of free list*/
    struct header *b = (struct header*)(HDRP(bp));
    b->next = (struct header*)heap_listp;
    if(b->next != NULL)
    {
	b->next->prev = b;
    }
	b->prev = NULL;
    heap_listp = (char*)b;

    /* Coalesce if the previous block was free */
    return coalesce(bp);
}

void print_free_list()
{
    struct header *b = (struct header*)heap_listp;
    while(b != NULL){
	printf("%u: %d (%s) \n", (int)b, b->size & ~0x7, (b->size & 0x1 ? "Not free" : "Free"));
	b = b->next;
    }
}

void print_all_list()
{
    char *bp;
    for (bp = (char*)mem_heap_lo() + 4*WSIZE; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp)){
 	printf("%u: %d (%s) \n", (int)bp, GET_SIZE(HDRP(bp)), (GET_ALLOC(HDRP(bp)) ? "allocated" : "free"));
    }
}

/*
 * add_to_list - Adds the block in front of the free list.
 */
void add_to_list(char* ptr)
{
    struct header *prev_front = (struct header*)heap_listp;
    struct header *new_front = (struct header*)ptr;

	/* The free list is not empty */
    if(prev_front != NULL)
    {
	prev_front->prev = new_front;
    }
    new_front->next = prev_front;
    new_front->prev = NULL;
    heap_listp = (char*)new_front;
    return;
}

/*
 * remove_from_list - Removes the block from the free list.
 */
void remove_from_list(char* ptr)
{
    struct header *b = (struct header*)ptr;

	/* The block is the only block in the list. */
    if(b->prev == NULL && b->next == NULL)
    {
	heap_listp = NULL;
	return;
    }

    if(b->prev != NULL)
    {
	b->prev->next = b->next;
    }
    if(b->next != NULL)
    {
	b->next->prev = b->prev;
    }
    if(b->prev == NULL && b->next != NULL)
    {
	heap_listp = (char*)b->next;
    }
    b->prev = NULL; b->next = NULL;
    return;
}

/*
* find_fit - Scans through the free list and returns a block
*     as soon as it finds a block that is big enough to fit
*     the payload (first fit).
*     If no suitable block is found, NULL is returned.
*/
void *find_fit(size_t size)
{
    struct header *b = (struct header*)heap_listp;
    while(b != NULL)
    {
		if(b->size >= size)
		{
	    	return (char*)b + WSIZE;
		}
		b = b->next;
    }
    return NULL;
}

/*
 * split_block - Splits the block into a given size and the rest of the block.
 *     If the rest of the block will be less than 16 bytes it does not split.
 */
void split_block(void* ptr, size_t size)
{
    size_t oldsize = GET_SIZE(HDRP((char*)ptr));
    if(oldsize - size < 16)
    {
	return;
    }
    remove_from_list(HDRP((char*)ptr));
    PUT(HDRP((char*)ptr), PACK(size, 0));
    PUT(FTRP((char*)ptr), PACK(size, 0));
    char* second = NEXT_BLKP((char*)ptr);
    PUT(HDRP(second), PACK(oldsize - size, 0));
    PUT(FTRP(second), PACK(oldsize - size, 0));
    add_to_list(HDRP((char*)ptr));
    add_to_list(HDRP(second));
    return;
}

/*
 * coalesce - boundary tag coalescing. Return ptr to coalesced block
 */
static void *coalesce(void *bp)
{
    size_t prev_alloc = GET_ALLOC(FTRP(PREV_BLKP(bp)));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));
    size_t size = GET_SIZE(HDRP(bp));

    if (prev_alloc && next_alloc) {            /* Case 1 */
        return bp;
    }

    else if (prev_alloc && !next_alloc) {      /* Case 2 */
	remove_from_list(HDRP(NEXT_BLKP(bp)));
	remove_from_list(HDRP(bp));
        size += GET_SIZE(HDRP(NEXT_BLKP(bp)));
        PUT(HDRP(bp), PACK(size, 0));
        PUT(FTRP(bp), PACK(size,0));
	add_to_list(HDRP(bp));
    }

    else if (!prev_alloc && next_alloc) {      /* Case 3 */
	remove_from_list(HDRP(PREV_BLKP(bp)));
	remove_from_list(HDRP(bp));
        size += GET_SIZE(HDRP(PREV_BLKP(bp)));
        PUT(FTRP(bp), PACK(size, 0));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
	add_to_list(HDRP(bp));
    }

    else {                                     /* Case 4 */
	remove_from_list(HDRP(NEXT_BLKP(bp)));
	remove_from_list(HDRP(PREV_BLKP(bp)));
	remove_from_list(HDRP(bp));
        size += GET_SIZE(HDRP(PREV_BLKP(bp))) +
            GET_SIZE(FTRP(NEXT_BLKP(bp)));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        PUT(FTRP(NEXT_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
	add_to_list(HDRP(bp));
    }

    return bp;
}

/* 
 * mm_checkheap - Check the heap for consistency 
 */
void mm_checkheap() 
{
	/* Point to prologue footer */
	char *bp = (char*)mem_heap_lo() + DSIZE;

	if (GET_SIZE(HDRP(bp)) != DSIZE || !GET_ALLOC(HDRP(bp)))
		if (verbose) printf("Bad prologue header (%d)\n", *(HDRP(bp)));

	/* Cycle through each block */
	for (bp = bp + DSIZE; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp))
	{
		if (verbose) printblock(bp);
		checkblock(bp);
	}

	if (verbose) printblock(bp);

	if (GET_SIZE(HDRP(bp)) != 0 || !GET_ALLOC(HDRP(bp)))
		if (verbose) printf("Bad epilogue header (%d)\n", *(HDRP(bp)));
}

static void printblock(void *bp) 
{
    size_t hsize, halloc, fsize, falloc;

    hsize = GET_SIZE(HDRP(bp));
    halloc = GET_ALLOC(HDRP(bp));  
    fsize = GET_SIZE(FTRP(bp));
    falloc = GET_ALLOC(FTRP(bp));  
    
    if (hsize == 0) {
	printf("%p: EOL\n", bp);
	return;
    }

    printf("%p: header: [%d:%c] footer: [%d:%c]\n", bp, 
	   hsize, (halloc ? 'a' : 'f'), 
	   fsize, (falloc ? 'a' : 'f')); 
}

static void checkblock(void *bp) 
{
    if ((size_t)bp % 8)
	printf("Error: %p is not doubleword aligned\n", bp);
    if (GET(HDRP(bp)) != GET(FTRP(bp)))
	printf("Error: header does not match footer\n");
}
